# openfoundry_tree

This is the OpenFoundry's Tree code. The tree is a virtual representation of the physical layout of a laboratory space. Transitions between two trees are human or robotic actions. In order to do anything, simply generate the desired tree representation of the end result and map transitions from the original tree to the end tree. 

This repo stores the OpenFoundry Tree objects and code assiocated with them.  
