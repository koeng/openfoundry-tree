import jsonpatch

original = {
  "baz": "qux",
  "foo": "bar"
}

patch = [
  { "op": "replace", "path": "/baz", "value": "boo", "test": "hi" },
  { "op": "add", "path": "/hello", "value": ["world"] },
  { "op": "remove", "path": "/foo" }
]

patch = jsonpatch.JsonPatch(patch)

result = patch.apply(original)

print(result)

move = {
        "action": "move",
        "target": "id",
        "new_parent": "id",
        "patch": []}

trash = {
        "action": "trash",
        "target": "id",
        "patch": []}

create = {
        "action": "create",
        "create_command": {},
        "patch": []}

transfer = {
        "action": "transfer",
        "from": "",
        "to": "",
        "patch": []}




