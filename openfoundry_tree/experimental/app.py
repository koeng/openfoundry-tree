from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
from flask import jsonify
from flask import request

import tree_create
from jsonschema import validate
import json
import jsonpatch
import uuid

from anytree.importer import JsonImporter
from anytree.exporter import JsonExporter, DictExporter
import anytree

world = json.dumps(tree_create.world['tree'])
importer = JsonImporter()
world = importer.import_(world)
walker = anytree.walker.Walker()

app = Flask(__name__)
api = Api(app)

# TODO add tree validation after every step

def response_to_json(response):
    return json.loads(response.data.decode('utf8').replace("'", '"'))

@app.route('/', methods=['GET'])
def root_world():
    return jsonify(DictExporter().export(world))

@app.route('/search/<string:id_type>/<string:id>', methods=['GET'])
def search(id_type,id):
    result_list = [] 
    search_result = anytree.search.findall_by_attr(world, name=id_type, value=id)
    for result in search_result:
        result_list.append(DictExporter().export(result))
    return jsonify(result_list)

@app.route('/search/locate/<string:id_type>/<string:id>', methods=['GET'])
def search_locate(id_type,id):
    result_list = []
    search_result = anytree.search.findall_by_attr(world, name=id_type, value=id)
    path = anytree.walker.Walker().walk(world,search_result[0])
    list_of_parent = []
    for node in path[-1]:
        print(node.parent.id)
        list_of_parent.append(node.parent.id)
    list_of_parent.append(search_result[0].id)
    return jsonify(list_of_parent)

@app.route('/check/<target>/<attribute>/<value>', methods=['GET'])
def check_value(target,attribute,value):
    result = response_to_json(search('id',target))[0]
    try:
        if result[attribute] == value:
            return jsonify(True)
        else:
            return jsonify(False)
    except:
        return jsonify(False)


###########
# Actions #
###########

@app.route('/hello', methods=['POST'])
def hello():
   return jsonify(request.json)

@app.route('/action/move', methods=['POST'])
def move():
    # Validate command
    move_validator = {
            "type": "object",
            "properties": {
                "target_id": {"type": "string"},
                "new_parent": {"type": "string"}
                },
            "required": [ "target_id", "new_parent" ],
            "maxProperties": 2
            }
    move_command = request.json
    if validate(move_command,move_validator) != None:
        return jsonify({'InvalidJson': {
            "message": "Move command requires ONLY target_id and new_parent",
            "status": 400}})
    # Validate future world
    movable_types = ['ot2','bench', 'node', 'plate']
    target_id = anytree.search.find_by_attr(world,name='id',value=move_command['target_id'])
    new_parent = anytree.search.find_by_attr(world,name='id', value=move_command['new_parent'])
    if target_id.node_type not in movable_types:
        return jsonify({'InvalidTree': {
            "message": "Attempted to move invalid type",
            "status": 400}})
    # Attach to tree
    target_id.parent = new_parent
    return jsonify({'Success': {
        "message": "Move successful",
        "status": 500}})

@app.route('/action/trash', methods=['POST'])
def post():
    # Validate command
    trash_validator = {
            "type": "object",
            "properties": {
                "target_id": {"type": "string"}
                },
            "required": [ "target_id"],
            "maxProperties": 1
            }
    trash_command = request.json
    if validate(trash_command,trash_validator) != None:
        return jsonify({'InvalidJson': {
            "message": "Trash command requires ONLY target_id",
            "status": 400}})
    trashable_types = ['ot2','bench','node','plate']
    target_id = anytree.search.find_by_attr(world.name='id',value=move_command['target_id'])
    if target_id.node_type not in trashable_types:
        return jsonify({'InvalidTree': {
            "message": "Attempted to trash invalid type",
            "status": 400}})
    # Detach from tree
    target_id.parent = None
    return jsonify({'Success': {
        "message": "Move successful",
        "status": 500}})

@app.route('/action/transfer', methods=['POST'])
def transfer():
    # Validate input
    transfer_validator = {
            "type": "object",
            "properties": {
                "from": {"type": "string"},
                "to": {"type": "string"},
                "volume": {"type": "number"},
                "tool": {"type": "object"},
                },
            "required": [ "from", "to", "volume", "tool"],
            "maxProperties": 4
            }
    transfer_command = request.json
    if validate(transfer_command,transfer_validator) != None:
        return jsonify({'InvalidJson': {
            "message": "Transfer command requires ONLY from, to, volume, tool",
            "status": 400}})
    # Validate types of from and to
    transfer_types = ['well']
    from_well = anytree.search.find_by_attr(world.name='id',value=move_command['from'])
    to_well = anytree.search.find_by_attr(world.name='id',value=move_command['to'])
    if from_well.node_type not in transfer_types or to_well.node_type not in transfer_types:
        return ({'InvalidTree': {
            "message": "Attempted to transfer invalid type",
            "status": 400}})
    # Check transfer location
    transfer_location_types = ['ot2','bench']
    if from_well.parent.parent.node_type not in transfer_location_types or to_well.parent.parent.node_type not in transfer_location_types:
        return ({'InvalidTree': {
            "message": "Attempted to transfer on invalid transfer_location type",
            "status": 400}})
    # Validate there is only one child to transfer in from
    if len(DictExporter().export(from_well)['children']) != 1:
        return ({'InvalidTree': {
            "message": "Well does not contain 1 child",
            "status": 400}})
    # Validate that the child of from is liquid
    if from_well.children[0].node_type not "liquid":
        return ({'InvalidTree': {
            "message": "Attempted to transfer a type other than liquid",
            "status": 400}})
    # Create new tree with only the single child liquid from from
    def replace_id(tree):
        input_json = DictExporter().export(tree)
        def new_id(input_json):
            input_json['id'] = str(uuid.uuid4())
            if 'children' in input_json:
                for child in input_json['children']:
                    new_id(child)
            return input_json
        return DictImporter().import_(new_id(input_json))
    pipetted_liquid = replace_id(from_well.children[0])

    # Finalize transfer
    pipetted_liquid.volume = volume
    pipetted_liquid.parent = to_well
    return jsonify({'Success': {
        "message": "Transfer successful",
        "status": 500}})


@app.route('/action/mix', methods=['POST'])
def mix():
    pass

@app.route('/action/create/node', methods=['POST'])
def create():
    # Validate command
    pass




if __name__ == '__main__':
    app.run(debug=True)
