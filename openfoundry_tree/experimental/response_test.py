import requests

response = requests.post('http://127.0.0.1:5000/hello', json={"foo": "bar"})
print(response.json())

response = requests.post('http://127.0.0.1:5000/action/move', json={"target_id": "44e1a6e1-1e35-46fa-a95b-a3ae922a7726", "new_parent": "351a1429-9479-4ec4-9c08-425d043c761a"})
print(response.json())
