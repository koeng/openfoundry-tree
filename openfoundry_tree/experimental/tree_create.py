from jsonschema import validate
import uuid

TreeValidator = {
  "$schema": "http://json-schema.org/draft-06/schema#",

  "definitions": {


    "FoundryBase": {
      "type": "object",
      "$id": "#FoundryBase",
      "properties": {
          "name" : {"type": "string", "maxLength": 50},
          "id": {"type": "string"},
          "description": {"type": "string"},
          "epoch" : {"type": "string", "format": "date-time"},
          },
      "required": ["id"],
      },
    "FoundryNode": {
        "type": "object",
        "$id": "#FoundryNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["node"]},
            "location": {"type": "array"},
            "potential_location": {"type": "array", "items": { "type": "array"}},
            "children": {
              "type": "array",
              "items": {  "oneOf": [
                  {"$ref": "#/definitions/FoundryNode"},
                  {"$ref": "#/definitions/PlateNode"},
                  ]},
              "default": []
              }
            },
        "required": ["node_type"]
        },


    "BenchNode": {
        "type": "object",
        "$id": "#BenchNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["bench"]},
            },
        "required": ["node_type"]
        },
    "OpentronsNode": {
        "type": "object",
        "$id": "#OpentronsNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["ot2"]},
            },
        "required": ["node_type"]
        },


    "PlateNode": {
        "type": "object",
        "$id": "#PlateNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "id": {"type": "string"},
            "node_type": {"type": "string", "enum": ["plate"]},
            "location": {"type": "array", "maxItems": 1},
            "potential_locations": {"type": "array", "items": { "type": "array"}},
            "thaw_count": {"type": "number"},
            "storage_conditions": {"type": "string", "enum": ["20c","4c","-20c","-80c"]},
            "plate_type": {"type": "string", "enum": ["standard96"]},
            "children": {
              "type": "array",
              "items": {  "oneOf": [
                  {"$ref": "#/definitions/WellNode"},
                  ]},
              "default": []
              }
            },
        "required": ["node_type","location","potential_location","thaw_count","storage_conditions","plate_type"]
        },
    "WellNode": {
        "type": "object",
        "$id": "#WellNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["well"]},
            "location": {"type": "array", "maxItems": 1},
            "sample_id": {"type": "string"},
            "children": {
              "type": "array",
              "items": {  "oneOf": [
                  {"$ref": "#LiquidNode"},
                  {"$ref": "#OrganismNode"},
                  {"$ref": "#DnaNode"},
                  ]},
              "default": []
              }
            },
        "required": ["node_type","location"]
        },


    "LiquidNode": {
        "type": "object",
        "$id": "#LiquidNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["liquid"]},
            "inventory_id": {"type": "string"},
            "state": {"type": "string", "enum": ["solid","liquid"]},
            "volume": {"type": "number"},
            "tags" : {"type": "string", "enum": ["","lb","2yt","H2O","glycerol"]},
            "children": {
              "type": "array",
              "items": {  "oneOf": [
                  {"$ref": "#OrganismNode"},
                  {"$ref": "#DnaNode"},
                  ]},
              "default": []
              }
            },
        "required": ["node_type"]
        },
    "OrganismNode": {
        "type": "object",
        "$id": "#OrganismNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["organism"]},
            "organism_id": {"type": "string"},
            "tags": {"type": "array"},
            "children": {
              "type": "array",
              "items": {  "oneOf": [
                {"$ref": "#DnaNode"},
                  ]},
              "default": []
              }
            },
        "required": ["node_type"]
        },
    "DnaNode": {
        "type": "object",
        "$id": "#DnaNode",
        "allOf": [{"$ref": "#/definitions/FoundryBase"}],
        "properties": {
            "node_type": {"type": "string", "enum": ["dna"]},
            "dna_id": {"type": "string"},
            "dna_fmol": {"type": "number"},
            "dna_type": {"type": "string", "enum": ["plasmid", "linear", "genomic"]},
            "tags": {"type": "array"},
              },
        "required": ["node_type"]
        },


    },
    "type": "object",
    "properties": {
            "tree": {"$ref": "#/definitions/FoundryNode"}
            }
}


def plate_positions(length, height):
    numbers = list(range(1, 1 + (length*height)))
    return [numbers[i:i + height] for i in range(0, len(numbers), height)]

world = { "tree": { 
    "id": "world",
    "name": "World",
    "description": "",
    "node_type": "node",
    "children": [
        {
            "plate_type": "standard96",
            "storage_conditions": "-20c",
            "thaw_count": 0,
            "location": [1],
            "potential_location": plate_positions(8,12),
            "id": "3fcc2caa-7ef8-43b0-84cd-31c35d461e94",
            "name": "Plate 1",
            "description": "A mad little plate",
            "node_type": "plate",
            "children": [
                {
                    "sample_id": "218c8159-cd13-4adf-a01c-876eebed107e",
                    "location": [1],
                    "id": "44e1a6e1-1e35-46fa-a95b-a3ae922a7726",
                    "name": "well A1",
                    "description": "A mad little well",
                    "node_type": "well",
                    "children": []
                }]
            },
        {
            "plate_type": "standard96",
            "storage_conditions": "-20c",
            "thaw_count": 0,
            "location": [2],
            "potential_location": plate_positions(8,12),
            "id": "351a1429-9479-4ec4-9c08-425d043c761a",
            "name": "Plate 2",
            "description": "A happy little plate",
            "node_type": "plate",
            "children": [
                {
                    "sample_id": "a6bb278b-583d-468e-84b7-cb3d6766f3de",
                    "location": [1],
                    "id": "b678d133-8b2b-46d1-8021-3290623c8af9",
                    "name": "well A1",
                    "description": "A happy little well",
                    "node_type": "well",
                    "children": []
                }]
            },

    ]
    }}

print(validate(world,TreeValidator))


