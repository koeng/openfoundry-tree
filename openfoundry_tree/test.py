import new_tree
import uuid

world = new_tree.WorldNode()
plate = new_tree.FoundryNode(name='Plate 1', description = 'A happy little plate', parent=world.get_node(id='world'))
well = new_tree.WellNode(sample_id = str(uuid.uuid4()),name='well A1', description = 'A happy little well', parent=world.get_node(name='Plate 1'))
liquid = new_tree.LiquidNode.from_json({'volume':100.0,'state':'liquid','name':'H2O','description': 'A happy splash of water', 'parent':world.get_node(name='Plate 1')})

plate2 = new_tree.PlateNode.from_json({'name':'Plate 2', 'description': 'A mad little plate', 'parent':world.get_node(id='world')})

print(world.json_export())

