from anytree import AnyNode, RenderTree, NodeMixin
import uuid
from anytree.exporter import DictExporter
from anytree import NodeMixin, RenderTree
from anytree.exporter import JsonExporter
from anytree import search
import datetime

#world = WorldNode(name='World', id='world', potential_locations=[[1]])

def timestamp() -> str:
    return str(datetime.datetime.now())

class FoundryBase(object):
    def __init__(self,id=None, name='',description='', node_type=None) -> None:
        if id == None:
            self.id:str = str(uuid.uuid4())
        else:
            self.id:str = id
        self.name:str = name
        self.description:str = description
        self.node_type:str = node_type

    @classmethod
    def from_json(cls, data):
        return cls(**data)


class FoundryNode(FoundryBase, NodeMixin):
    def __init__(self,parent=None,*args,**kwargs) -> None:
        self.parent = parent
        super(FoundryNode,self).__init__(*args,**kwargs)

    def render(self):
        for pre, _, node in RenderTree(self):
            treestr = u"%s%s" % (pre, node.name)
            print(treestr.ljust(8), '(' + str(node.location) + ')', node.id)

    def search_tree(self, attr, value, render=False):
        search_result = search.find_by_attr(self, name=attr, value=value)
        if search_result == None:
            raise ValueError("Search result not found")
        if render == True: world.render()
        return search_result

    def get_node(self, name:str=None, id:str=None, render:bool=False): # ->
        """A function to get the id of a node given the name"""
        if not name == None:
            return self.search_tree("name", name, render=render)
        elif not id == None:
            return self.search_tree("id", id, render=render)
        else:
            raise ValueError("No name or id given.")

    def json_export(self):
        exporter = JsonExporter()
        return exporter.export(self)

class WorldNode(FoundryNode):
    def __init__(self, history=[],*args,**kwargs):
        self.history:list = history
        super(WorldNode,self).__init__(id='world',name='World',node_type='world',*args,**kwargs)

    def recover(self, history:dict) -> None:
        for command in history["history"]:
            self.action(command)

    def move(self, json_command):
        target = self.get_node(id=json_command["target_id"])
        parent_node = self.get_node(id=json_command["to"])
        location = json_command["location"]
        # Check if location is already occupied
        self.check_locations(parent_node, location)
        # Apply changes
        target.parent = parent_node
        target.location = location
    
    def trash(self, json_command):
        json_command["to"] = "trash"
        json_command["location"] = None
        self.move(json_command)

    def transfer(self, json_command):
        pass
    
    def update(self, json_command):
        pass

    def action(self, json_command, job_id=None, author=None, email=None): # Implement transfer
        for task in json_command["tasks"]:
            # Do requested action
            if task["command"] == "MOVE":
                self.move(task)
            if task["command"] == "TRASH":
                self.trash(task)
                task["command"] = "MOVE"
            if task["command"] == "CREATE":
                self.create(task)
            # Log the action
        if json_command.get("completed_by") == None:
            json_command["completed_by"] = {"completed_by": author,
                    "completed_email" : email,
                    "timestamp" : timestamp() }
        root_node = self.get_node(id='world')
        root_node.history.append(json_command)
        return True

    def backup(self):
        return {"history": self.history}

class FoundryContainer(FoundryNode):
    def __init__(self,location=[],potential_locations=[],*args,**kwargs) -> None:
        self.location:list = location
        self.potential_locations:list = potential_locations
        super(FoundryContainer,self).__init__(*args,**kwargs)

####################
# Plates and wells #
####################

class WellNode(FoundryNode):
    def __init__(self,sample_id,location=[],*args,**kwargs):
        self.sample_id:str = sample_id
        self.location:list = location
        super(WellNode,self).__init__(node_type='well',*args,**kwargs)


def plate_positions(length, height):
    numbers = list(range(1, 1 + (length*height)))
    return [numbers[i:i + height] for i in range(0, len(numbers), height)]

def convert_address(address):
    rows = ['A','B','C','D','E','F','G','H']
    return (rows.index(address[0])*12) + int(address[1:])

class PlateNode(FoundryContainer):
    def __init__(self,plate_type="Standard96",storage_conditions='-20c',thaw_counter=0,potential_locations=plate_positions(8,12),*args,**kwargs):
        self.plate_type:str = plate_type
        self.storage_conditions:str = storage_conditions
        self.thaw_counter:int = thaw_counter
        super(PlateNode,self).__init__(node_type='plate',*args,**kwargs)


###########
# Samples #
###########

class SampleNode(FoundryNode):
    def __init__(self,tags=[],*args,**kwargs):
        self.tags:list = tags
        super(SampleNode,self).__init__(*args,**kwargs)

class DnaNode(SampleNode):
    def __init__(self,dna_id,dna_type,picomolar,*args,**kwargs):
        self.dna_id:str = dna_id
        self.dna_type:str = dna_type
        self.picomolar:float = picomolar
        super(DnaNode,self).__init__(node_type='dna',*args,**kwargs)

class OrganismNode(SampleNode):
    def __init__(self,organism_id,*args,**kwargs):
        self.organism_id:str = organism_id
        super(OrganismNode,self).__init__(node_type='organism',*args,**kwargs)

class LiquidNode(SampleNode):
    def __init__(self,volume,state='liquid',*args,**kwargs):
        self.volume:float = volume
        self.state:str = state
        super(LiquidNode,self).__init__(node_type='liquid',*args,**kwargs)


