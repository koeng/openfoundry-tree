import json
import unittest
import tree
import sys
import uuid
import numpy as np

# Setup world
world = tree.WorldNode(name='World', id='world', potential_locations=[[1,2,3,4]])

plate_locs = tree.Plates.StandardPlate().positions

bench_id = str(uuid.uuid4())
freezer_20 = str(uuid.uuid4())
freezer_80 = str(uuid.uuid4())
freezer_80_backup = str(uuid.uuid4())

shelf = str(uuid.uuid4())
rack = str(uuid.uuid4())
koengtron = str(uuid.uuid4())

create_commands= [
    {"command": "CREATE",
    "type": "NODE",
    "target_id": bench_id,
    "name": "Keoni's Bench",
    "location": 1,
    "potential_locations": [[1,2,3]],
    "parent": world.id},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": freezer_20,
    "name": "Main -20c Freezer",
    "location": 2,
    "potential_locations": [[1],
        [2],
        [3],
        [4]],
    "parent": world.id},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": freezer_80,
    "name": "Main -80c Freezer",
    "location": 3,
    "potential_locations": [[1],
        [2],
        [3],
        [4]],
    "parent": world.id},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": freezer_80_backup,
    "name": "Backup -80c Freezer",
    "location": 4,
    "potential_locations": [[1],
        [2],
        [3],
        [4]],
    "parent": world.id},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": koengtron,
    "name": "KoengTron",
    "location": 3,
    "potential_locations": [[1,2,3,4,5],
        [6,7,8,9,10],
        [11,12,13,14,15]],
    "parent": bench_id},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": shelf,
    "name": "-20c bottom shelf",
    "location": 4,
    "potential_locations": [[1,2,3,4]],
    "parent": freezer_20},

    {"command": "CREATE",
    "type": "NODE",
    "target_id": rack,
    "name": "Keoni's Bench",
    "location": 1,
    "potential_locations": [[5],
        [4],
        [3],
        [2],
        [1]], 
    "parent": shelf}
    ]

request_id = str(uuid.uuid4())
job = {"command": "JOB",
        "parent_request": request_id,
        "job_id": str(uuid.uuid4()),
        "job_name": "Build lab",
        "job_description": "Build the lab world according to json inputs.",
        "completed_by": {
            "name": "",
            "email": "",
            "date": ""},
        "tasks": create_commands
        }

request = {"command": "REQUEST",
        "request_id": request_id,
        "request_name": "Build lab",
        "request_description": "Please execute job 'Build lab' to complete this request.",
        "requested_by": {
            "name": "Keoni Gandall",
            "email": "koeng101@gmail.com",
            "date": "timestamp"},
        "completion_date": "timestamp",
        "requested_jobs": [job],
        "completed_jobs": []}

# Apply to world
print("ADDING REQUEST TO WORLD")
world.requests.append(request)
world.render()

print("SIMPLE EXECUTOR")
world.simple_executor()

print("RENDERING WORLD")
world.render()

print(world.history)

print("REMOVING HISTORY")
world.history = []

print(world.json_export())


sys.exit()
# Create a node
print("Creating a node")
parent_node = world.get_node(name='Freezer').id
create_node = {"command": "CREATE",
    "type": "NODE",
    "target_id": None,
    "name": "Plate 40",
    "location": 3,
    "potential_locations": [],
    "parent": parent_node}
world.action(create_node)
world.render()

print("Move a node")
parent_node = world.get_node(name='KoengTron').id
move_node = world.get_node(name='Plate 40').id
move = {"command": "MOVE",
    "target_id": move_node,
    "to": parent_node,
    "location": None}
world.action(move)
world.render()

print("Trash a node")
trash = {"command": "TRASH",
    "target_id": move_node}
world.action(trash)
world.render()

print("Print history")
print(world.history)

print("Show locations")
freezer = world.get_node(name='Freezer')
freezer.show_matrix()

print("Export world")
print(world.json_export())

print("Print render")
print(world.id)
