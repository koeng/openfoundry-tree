from tinydb import TinyDB, Query
from part import *

sample = Part(name="GFP", description="Default description", author="Keoni Gandall", part_type="cds", sequence="ATGTAA", tags=['Cool'], status='Requested', derived_from=[])

print(sample.make_dict())


db = TinyDB('db.json')
db.insert(sample.make_dict())

