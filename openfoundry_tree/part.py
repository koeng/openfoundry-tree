import datetime
import uuid
import mypy
import uuid
from enum import Enum
import json
from typing import List, Dict
import re
import json

def create_sample(plasmid_id:str,status:str,derived_from:str='',pileup:dict={},id:str=str(uuid.uuid4()),version:str='1.0.0') -> dict:
    ### Possible status: unsequenced, sequence_confirmed, mutation, failed
    return {
        'id': id,
        'version': version,
        'plasmid_id': plasmid_id,
        'status': status,
        'derived_from': derived_from, # Another sample
        'pileups': [
            {'date': '',
                'description': '',
                'pileup': []}
            ]
        }

def pileup_to_json_pileup(pileup): # Position, Sequence, Reference Base, Read Count, Read Results, Quality 
    data = []
    for line in pileup.split('\n'):
        sp=line.split()
        data.append({'Position':sp[0],'Sequence':sp[1],'Reference_Base':sp[2],'Read_Count':sp[3],'Read_Results':sp[4],'Quality':sp[4]})
    return data

def json_pileup_to_pileup(pileup): # Position, Sequence, Reference Base, Read Count, Read Results, Quality
    data = []
    for base in pileup:
        line = '{}\t{}\t{}\t{}\t{}\t{}'.format(base['Position'],base['Sequence'],base['Reference_Base'],base['Read_Count'],base['Read_Results'],base['Quality'])
        data.append(line)
    return '\n'.join(data)
    

def create_virtual_organism(organism_name:str,genotype:str,tags:list,id:str=str(uuid.uuid4()),version:str='1.0.0'):
    return {
        'id': id,
        'version': version,
        'organism_name': organism_name,
        'genotype': genotype,
        'tags': tags, 
        }


def create_part(gene_id:str,name:str,description:str,part_type:str,sequence:str,tags:list,id:str=str(uuid.uuid4()),version:str='1.0.0') -> dict:
    return {
        'id': id,
        'version': version,
        'type': 'PART',
        'gene_id': gene_id,
        'name': name,
        'description': description,
        'part_type': part_type,
        'sequence': sequence.upper(),
        'tags': tags, # 'antibiotic:ampicillin:100', 'negative_selection:ccdb', 'strain_required:ccdb_res'
    }

def create_plasmid(derived_from:List[dict],contains:dict,enzyme:str,tags:list,sequence=None,id:str=str(uuid.uuid4()),version:str='1.0.0') -> dict:
    # Derive sequences
    if sequence == None:
        sequence = simulate_cloning(enzyme, derived_from)
    if not contains['sequence'] in sequence:
        raise ValueError('Final sequence not in plasmid')
    # Import tags
    for part in derived_from:
        if part['part_type'] == 'vector':
            new_tags = list(filter(lambda item: 'negative_selection' not in item, part['tags']))
            tags += new_tags
        else:
            tags += part['tags']
    tags += contains['tags']
    tags = list(set(tags))
    # Derived from ids
    derived_ids = []
    for part in derived_from:
        derived_ids.append(part['id'])
    return {
        'id': id,
        'version': version,
        'name': 'p' + contains['gene_id'],
        'type': 'PLASMID',
        'derived_from': derived_ids,
        'contains': contains['id'],
        'enzyme': enzyme,
        'sequence': sequence.upper(),
        'tags': tags,
    }

enzymes = {
    "bbsi" : {
        "seq" : "GAAGAC",
        "jump" : 2,
        "overhang" : 4
        },
    "bsai" : {
        "seq" : "GGTCTC",
        "jump" : 1,
        "overhang" : 4
        },
    "btgzi" : {
        "seq" : "GCGATG",
        "jump" : 10,
        "overhang" : 4
        },
    }
def reverse_complement(seq):
    return seq.translate(str.maketrans("ATGCN","TACGN"))[::-1]

def simulate_cloning(enzyme:str,parts:list) -> str:
    if not enzyme in enzymes:
        raise ValueError('Enzyme not in enzyme list')
    # Parse out sequence data
    part_sequences=[]
    for part in parts: 
        sequence = part['sequence'].upper()
        enzyme_seq = enzymes[enzyme]['seq']
        remove_ends = enzymes[enzyme]['jump']
        result = re.findall('{}(.*){}'.format(enzyme_seq, reverse_complement(enzyme_seq)), sequence)[0][remove_ends:-remove_ends]
        if part['part_type'] == 'vector': # Split vector on ccdB
            vector = result
        else:
            seq_with_overhang = result
            part_sequences.append(seq_with_overhang)
    part_sequences.append(vector)
    new_part = part_sequences
    # Check for unique overhangs
    def unique_overhangs(new_part:list) -> None:
        fiveprime = []
        threeprime = []
        for part in new_part:
            fiveprime.append(part[:4])
            threeprime.append(part[-4:])
        if len(set(fiveprime)) < len(fiveprime):
            raise ValueError('Duplicate fiveprime - {}'.format(fiveprime))
        if len(set(threeprime)) < len(threeprime):
            raise ValueError('Duplicate threeprime - {}'.format(threeprime))
    unique_overhangs(new_part)
    # Build sequence
    counter = 0
    while len(new_part) > 1:
        if counter > 50:
            raise ValueError('Gene cannot be simulated in 50 rounds')
        counter +=1
        for part in new_part:
            if part[-4:] == new_part[-1][:4]: # TODO check five_prime as well
                new_part[-1] = part + new_part[-1][4:]
                new_part.remove(part)
    return new_part[0]




