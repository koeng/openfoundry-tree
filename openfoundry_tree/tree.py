from anytree import AnyNode, RenderTree, NodeMixin
import uuid
from enum import Enum
from anytree.exporter import DictExporter
from anytree import NodeMixin, RenderTree
from anytree.exporter import JsonExporter
from anytree import search
import datetime

import numpy as np


# Each object has a method to CREATE, a method to store as a dict, and a method to restore from a dict. 


class EnumString(Enum):
    def __str__(self):
        return str(self.value)

def timestamp():
    return str(datetime.datetime.now())

def option_list(options):
    return options[int(input(''.join([str(counter) + ". " + str(option) + "\n" for counter, option in list(enumerate(options, start=1))]))) - 1]


class FoundryBase(object):
    def __init__(self, id, name=None, location=[], potential_locations=[], node_type='node'):
        self.location: list = location
        self.node_type: str = node_type
        self.name: str = name
        self.epoch = timestamp()
        self.potential_locations: list = potential_locations
        if id == None:
            self.id = str(uuid.uuid4())
        else:
            self.id = id

# CREATE, MOVE, EDIT, DELETE
# TRANSFER, WAIT

class FoundryNode(FoundryBase, NodeMixin):
    def __init__(self, id=None, name=None, location=[], parent=None, potential_locations=[], node_type='node'):
        super(FoundryNode,self).__init__(id=id,name=name,location=location, potential_locations=potential_locations, node_type=node_type) # add potential locations
        self.parent = parent
        
        # Check location
        #if not self.parent == None:
        #    self.check_locations(parent, None) # None is given since the location since class is already established as a child of parent

    def render(self):
        for pre, _, node in RenderTree(self):
            treestr = u"%s%s" % (pre, node.name)
            print(treestr.ljust(8), '(' + str(node.location) + ')', node.id)

    # TODO make show_matrix arbitrary to allow for strings as well as integers to be in matrixs
    def show_matrix(self):
        arr = np.array(self.potential_locations, dtype='object')
        for child in self.children:
            arr[arr == child.location] = child.name
        print(arr) 

    def search_tree(self, attr, value, render=False):
        search_result = search.find_by_attr(self, name=attr, value=value)
        if search_result == None:
            raise ValueError("Search result not found")
        if render == True: world.render()
        return search_result

    def get_node(self, name=None, id=None, render=False):
        """A function to get the id of a node given the name"""
        if not name == None:
            return self.search_tree("name", name, render=render)
        elif not id == None:
            return self.search_tree("id", id, render=render)
        else:
            raise ValueError("No name or id given.")
    
    def view_node(self, uuid, render=False):
        """Returns node of uuid"""
        return self.search_tree('id', uuid, render=render)

    def json_export(self):
        exporter = JsonExporter()
        return exporter.export(self)

    # TODO change to accept location lists
    def check_locations(self, parent_node, new_location):
        current_locations = [new_location]
        for child in parent_node.children:
            current_locations.append(child.location)
        current_locations = [x for x in current_locations if x is not None]
        if len(current_locations) != len(set(current_locations)):
            raise ValueError("Location already exists")
    
    # TODO UUID validator

    # TODO locations as a matrix - location validator

class WorldNode(FoundryNode):
    def __init__(self, id='world', name='World', location=[], parent=None, potential_locations=[], history=[], requests=[]):
        super(WorldNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=potential_locations, node_type='world')
        self.history = history
        self.requests = []
    
    def recover(self, history:dict) -> None:
        for command in history["history"]:
            self.action(command)

    def move(self, json_command):
        target = self.get_node(id=json_command["target_id"])
        parent_node = self.get_node(id=json_command["to"])
        location = json_command["location"]
        # Check if location is already occupied
        self.check_locations(parent_node, location)
        # Apply changes
        target.parent = parent_node
        target.location = location

    def trash(self, json_command):
        json_command["to"] = "trash"
        json_command["location"] = None
        self.move(json_command)

    def create(self, json_command):
        if json_command["type"] == "NODE":
            FoundryNode(id=json_command["target_id"], name=json_command["name"], location=json_command["location"], potential_locations=json_command["potential_locations"], parent=self.get_node(id=json_command["parent"]))
        if json_command["type"] == "OPENTRONS":
            OpenTronsNode(id=json_command["target_id"], name=json_command["name"], location=json_command["location"], left_mount=json_command["left_mount"], right_mount=json_command["right_mount"], parent=self.get_node(id=json_command["parent"]))
        if json_command["type"] == "PLATE":
            PlateNode(id=json_command["target_id"], name=json_command["name"], location=json_command["location"], parent=self.get_node(id=json_command["parent"]), plate_type=json_command["plate_type"], storage_conditions=json_command["storage_conditions"])
        if json_command["type"] == "WELL":
            WellNode(id=json_command["target_id"], name=json_command["name"], location=json_command["location"],parent=self.get_node(id=json_command["parent"]))
        if json_command["type"] == "DNA":
            DnaNode(id=json_command["target_id"], name=json_command["name"], parent=self.get_node(id=json_command["parent"]), dna_id=json_command["dna_id"], dna_type=json_command["dna_type"], fmols=json_command["fmols"], sample_id=json_command["sample_id"], tags=json_command["tags"])
        if json_command["type"] == "ORGANISM":
            OrganismNode(id=json_command["target_id"], name=json_command["name"], parent=self.get_node(id=json_command["parent"]), organism_id=json_command["organism_id"], tags=json_command["tags"])
        if json_command["type"] == "LIQUID":
            LiquidNode(id=json_command["target_id"], name=json_command["name"], parent=self.get_node(id=json_command["parent"]), state=json_command["state"], volume=json_command["volume"])


    def transfer(self,json_command):
        liquid_from = self.get_node(id=json_command["target_id"])
        liquid_to = self.get_node(id=json_command["target_id"])
        #if not isinstance(liquid_from.parent.parent.parent, OpenTrons) and isinstance(liquid_to.parent.parent.parent, OpenTrons):

    def action(self, json_command, job_id=None, author=None, email=None): # Implement transfer
        for task in json_command["tasks"]:
            # Do requested action
            if task["command"] == "MOVE":
                self.move(task)
            if task["command"] == "TRASH":
                self.trash(task)
                task["command"] = "MOVE"
            if task["command"] == "CREATE":
                self.create(task)
            # Log the action
        if json_command.get("completed_by") == None:
            json_command["completed_by"] = {"completed_by": author,
                    "completed_email" : email,
                    "timestamp" : timestamp() }
        root_node = self.get_node(id='world')
        root_node.history.append(json_command)
        return True

    def backup(self):
        return {"history": self.history}


#############
# OpenTrons #
#############

class OpenTronsNode(FoundryNode):
    def __init__(self,id=None,name=None,location=[],parent=None,potential_locations=[[10,11,'Trash'],[7,8,9],[4,5,6],[1,2,3]], left_mount='p10_multi', right_mount='p300_multi'):
        super(OpenTronsNode,self).__init__(id=id,name=name,parent=parent,location=location,potential_locations=potential_locations,node_type='OPENTRONS')

        # Initialize robot
        self.robot={"model": "OT-2 Standard"}

        # Initialize opentrons_type
        self.right_mount = {"pipette1Id": 
                {"type": "pipette",
                    "mount": "right",
                    "model": right_mount}}
        self.left_mount = {"pipette2Id":
                {"type": "pipette",
                    "mount": "left",
                    "model": left_mount}}
        if left_mount == 'p10_multi' and right_mount == 'p300_multi':
            self.opentrons_type = 'plater'
        if left_mount == 'p10_single' and right_mount == 'p300_single':
            self.opentrons_type = 'picker'
        else:
            self.opentrons_type = 'unknown'
    
    def as_dict(self):
        return {"id": self.id,
                "name": self.name,
                "children": [x.as_dict() for x in self.children]}
    @classmethod
    def from_json(cls, json_import):
        return cls(id=json_import['id'],name=json_import['name'],location=json_import['location'],parent=json_import['parent'],potential_locations=json_import['potential_locations'], left_mount=json_import['left_mount'], right_mount=json_import['right_mount'])


####################
# Plates and wells #
####################

class WellNode(FoundryNode):
    def __init__(self, id=None, name=None, location=[], parent=None, well_type=None, sample_id=None):
        super(WellNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=[], node_type='well')
        self.sample_id=sample_id
        # TODO enforce wells can only have one location
   

class PlateNode(FoundryNode):
    def __init__(self, id=None, name=None, location=[], parent=None, plate_type=None, storage_conditions=None, potential_locations=[]):
        super(PlateNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=potential_locations, node_type='plate')
        self.plate_type = plate_type
        if self.plate_type == "Standard96": # TODO enforce enum 
            self.potential_locations = plate_positions(8,12)
        if self.plate_type == "Pcr96":
            self.potential_locations = plate_positions(8,12)
        self.storage_conditions = storage_conditions # TODO enforce enum RT, 4c, -20c, -80c

###########
# Samples #
###########

class DnaNode(FoundryNode):
    def __init__(self, id=None, name=None, location=[], parent=None, potential_locations=[], sample_id=None, dna_id=None, dna_type='part', fmols=0, tags=[]):
        super(DnaNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=potential_locations, node_type='dna')
        self.dna_id:str=dna_id
        self.dna_type:str=dna_type # plasmid, part
        self.fmols:float=fmols
        self.tags:list=tags

        self.sample=sample_id

class OrganismNode(FoundryNode):
    def __init__(self, id=None, name=None, location=[], parent=None, potential_locations=[], organism_id=None, tags=[]):
        super(OrganismNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=potential_locations, node_type='organism')
        self.organism_id=organism_id
        self.tags:list=tags

class LiquidNode(FoundryNode):
    def __init__(self, id=None, name=None, location=[], parent=None, potential_locations=[], state='liquid', volume=None):
        super(LiquidNode,self).__init__(id=id, name=name, parent=parent, location=location, potential_locations=potential_locations, node_type='liquid')
        self.state=state
        self.volume:float=float(volume) # Vol in ul

##########
# Plates #
##########

# TODO make horizontal and vertical stacking for if you want to stack by columns or rows
def plate_positions(length, height):
    numbers = list(range(1, 1 + (length*height)))
    return [numbers[i:i + height] for i in range(0, len(numbers), height)]

def convert_address(address):
    rows = ['A','B','C','D','E','F','G','H']
    return (rows.index(address[0])*12) + int(address[1:])

class Plates():
    class StandardPlate():
        def __init__(self):
            self.positions = plate_positions(12,8)  

#########
# World #
#########

world = WorldNode(name='World', id='world', potential_locations=[[1]])

####################
# Create functions #
####################

# TODO add descriptions
# OpenTrons and Node
def create_node(parent:str, name:str, location:list, potential_locations:list, id=str(uuid.uuid4())) -> dict:
    return {"command": "CREATE",
    "type": "NODE",
    "target_id": id,
    "name": name,
    "location": location,
    "potential_locations": potential_locations,
    "parent": parent}

def create_opentrons(parent:str, name:str, location:list, left_mount='p10_multi', right_mount='p300_multi', id=str(uuid.uuid4())) -> dict:
    return {"command": "CREATE",
    "type": "OPENTRONS",
    "target_id": id,
    "name": name,
    "location": location,
    "left_mount": left_mount,
    "right_mount": right_mount,
    "parent": parent}

# Samples
def create_liquid(parent:str, volume:float, name="H2O", state="liquid", id=str(uuid.uuid4())) -> dict:
   return {"command": "CREATE",
   "type": "LIQUID",
   "target_id": id,
   "name": name,
   "state": state,
   "volume": volume,
   "parent": parent}

def create_dna(parent, name:str, dna_id:str, dna_type:str, fmols:float, tags:list, sample_id=None, id=str(uuid.uuid4())) -> dict:
   return {"command": "CREATE",
   "type": "DNA",
   "target_id": id,
   "name": name,
   "dna_id": dna_id,
   "dna_type": dna_type, # synthetic_fragment, plasmid
   "fmols": fmols,
   "tags": tags,
   "sample_id": sample_id,
   "parent": parent}

def create_organism(parent, name:str, organism_id:str, tags:list, id=str(uuid.uuid4())) -> dict:
    return {"command": "CREATE",
   "type": "ORGANISM",
   "target_id": id,
   "name": name,
   "organism_id": organism_id,
   "tags": tags,
   "parent": parent}

# Wells and plates
def create_well(parent:str, location:list, id=str(uuid.uuid4())) -> dict:
   return {"command": "CREATE",
   "type": "WELL",
   "target_id": id,
   "name": "{} well {}".format(parent,location),
   "location": location,
   "parent": parent}

def create_plate(parent:str, location:list, name:str, plate_type:str, storage_conditions, id=str(uuid.uuid4()), thaw_count=0) -> dict:
   return {"command": "CREATE",
   "type": "PLATE",
   "target_id": id,
   "name": name,
   "location": location,
   "plate_type": plate_type,
   "storage_conditions": storage_conditions,
   "thaw_count": thaw_count,
   "parent": parent}

def create_job(job_name:str, description:str, tasks:list, id=str(uuid.uuid4()), request_id=None) -> dict:
    return {"command": "JOB",
        "parent_request": request_id,
        "job_id": id,
        "job_name": job_name,
        "job_description": description,
        "tasks": tasks 
        }


